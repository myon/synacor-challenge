#!/usr/bin/python3

import itertools

coins = {
        'red': 2,
        'corroded': 3,
        'shiny': 5,
        'concave': 7,
        'blue': 9,
        }

for p in itertools.permutations(coins.keys()):
    if coins[p[0]] + coins[p[1]] * coins[p[2]]**2 + coins[p[3]]**3 - coins[p[4]] == 399:
        for c in p:
            print("use", c, "coin")
