#!/usr/bin/python3

debug = False

import array
import sys

mem = array.array('H') # unsigned short
stack = array.array('H') # unsigned short

with open('challenge.bin', 'rb') as f:
    while True:
        word = f.read(2)
        if len(word) == 0:
            break
        mem.frombytes(word)

pc = 0
reg = [0 for x in range(8)]

class InvalidOpcode(BaseException):
    pass

class InvalidValue(BaseException):
    pass

def val(i):
    if 0 <= i < 32768:
        return i
    elif 32768 <= i < 32768 + 8:
        return reg[i - 32768]
    else:
        raise InvalidValue("invalid value {}".format(i))

def out(opcode, *args, result=None):
    if not debug: return
    print(pc, ": ", opcode, sep="", end="")
    for arg in args:
        if 32768 <= arg:
            print(" r{}[{}]".format(arg - 32768, reg[arg - 32768]), end="")
        else:
            print(" {}".format(arg), end="")
    if result is not None:
        print(" -> {}".format(result), end="")
    print("")

while True:
    instr = mem[pc]
    a = mem[pc + 1]
    b = mem[pc + 2]
    c = mem[pc + 3]
    # halt: 0
    #   stop execution and terminate the program
    if instr == 0:
        out("halt")
        exit(0)
    # set: 1 a b
    #   set register <a> to the value of <b>
    elif instr == 1:
        out("set", a, b)
        reg[a - 32768] = val(b)
        pc += 3
    # push: 2 a
    #   push <a> onto the stack
    elif instr == 2:
        out("push", a)
        stack.append(val(a))
        pc += 2
    # pop: 3 a
    #   remove the top element from the stack and write it into <a>; empty stack = error
    elif instr == 3:
        result = stack.pop()
        out("pop", a, result=result)
        reg[a - 32768] = result
        pc += 2
    # eq: 4 a b c
    #   set <a> to 1 if <b> is equal to <c>; set it to 0 otherwise
    elif instr == 4:
        result = val(b) == val(c)
        out("eq", a, b, c, result=result)
        reg[a - 32768] = result
        pc += 4
    # gt: 5 a b c
    #   set <a> to 1 if <b> is greater than <c>; set it to 0 otherwise
    elif instr == 5:
        result = val(b) > val(c)
        out("gt", a, b, c, result=result)
        reg[a - 32768] = result
        pc += 4
    # jmp: 6 a
    #   jump to <a>
    elif instr == 6:
        out("j", a)
        pc = val(a)
    # jt: 7 a b
    #   if <a> is nonzero, jump to <b>
    elif instr == 7:
        out("jt", a, b)
        if val(a) != 0:
            pc = val(b)
        else:
            pc += 3
    # jf: 8 a b
    #   if <a> is zero, jump to <b>
    elif instr == 8:
        out("jf", a, b)
        if val(a) == 0:
            pc = val(b)
        else:
            pc += 3
    # add: 9 a b c
    #   assign into <a> the sum of <b> and <c> (modulo 32768)
    elif instr == 9:
        result = (val(b) + val(c)) % 32768
        out("add", a, b, c, result=result)
        reg[a - 32768] = (val(b) + val(c)) % 32768
        pc += 4
    # mult: 10 a b c
    #   store into <a> the product of <b> and <c> (modulo 32768)
    elif instr == 10:
        out("mult", a, b, c)
        reg[a - 32768] = (val(b) * val(c)) % 32768
        pc += 4
    # mod: 11 a b c
    #   store into <a> the remainder of <b> divided by <c>
    elif instr == 11:
        out("mod", a, b, c)
        reg[a - 32768] = val(b) % val(c)
        pc += 4
    # and: 12 a b c
    #   stores into <a> the bitwise and of <b> and <c>
    elif instr == 12:
        out("and", a, b, c)
        reg[a - 32768] = val(b) & val(c)
        pc += 4
    # or: 13 a b c
    #   stores into <a> the bitwise or of <b> and <c>
    elif instr == 13:
        out("or", a, b, c)
        reg[a - 32768] = val(b) | val(c)
        pc += 4
    # not: 14 a b
    #   stores 15-bit bitwise inverse of <b> in <a>
    elif instr == 14:
        out("not", a, b)
        reg[a - 32768] = ~val(b) % 32768
        pc += 3
    # rmem: 15 a b
    #   read memory at address <b> and write it to <a>
    elif instr == 15:
        m = mem[val(b)]
        out("rmem", a, b, result=m)
        #if val(b) in (2732, 2733):
        #    m = 2317
        #else:
        reg[a - 32768] = m
        pc += 3
    # wmem: 16 a b
    #   write the value from <b> into memory at address <a>
    elif instr == 16:
        out("wmem", a, b, result=repr(chr(val(b))))
        mem[val(a)] = val(b)
        pc += 3
    # call: 17 a
    #   write the address of the next instruction to the stack and jump to <a>
    elif instr == 17:
        out("call", a)
        if val(a) != 6027:
            stack.append(pc + 2)
            pc = val(a)
        else:
            reg[0] = 6
            pc += 2
    # ret: 18
    #   remove the top element from the stack and jump to it; empty stack = halt
    elif instr == 18:
        result = stack.pop()
        out("ret", result=result)
        pc = result
    # out: 19 a
    #   write the character represented by ascii code <a> to the terminal
    elif instr == 19:
        char = chr(val(a))
        print(char, end='')
        out("out", a, result=repr(char))
        pc += 2
    # in: 20 a
    #   read a character from the terminal and write its ascii code to <a>; it can be assumed that once input starts, it will continue until a newline is encountered; this means that you can safely read whole lines from the keyboard and trust that they will be fully read
    elif instr == 20:
        i = sys.stdin.read(1)
        if i == 'X':
            print("Set r7 to 6")
            reg[7] = 6
            #debug = True
            i = sys.stdin.read(1)
        reg[a - 32768] = ord(i)
        pc += 2
    # noop: 21
    #  no operation
    elif instr == 21:
        out("noop")
        pc += 1
    else:
        raise InvalidOpcode("pc {}, instr {} [{}] {}".format(pc, mem[pc-8:pc-1], mem[pc], mem[pc+1:pc+8]))
