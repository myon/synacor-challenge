#!/usr/bin/python3

import array
import sys

mem = array.array('H') # unsigned short
stack = array.array('H') # unsigned short

with open('challenge.bin', 'rb') as f:
    while True:
        word = f.read(2)
        if len(word) == 0:
            break
        mem.frombytes(word)
# add fake data because we access pc+1/2/3 for a/b/c unconditionally
mem.append(0)
mem.append(0)
mem.append(0)
mem.append(0)

pc = 0

class InvalidOpcode(BaseException):
    pass

while True:
    instr = mem[pc]
    a = mem[pc + 1]
    if a >= 32768:
        a = "r{}".format(a - 32768)
    b = mem[pc + 2]
    if b >= 32768:
        b = "r{}".format(b - 32768)
    c = mem[pc + 3]
    if c >= 32768:
        c = "r{}".format(c - 32768)
    # halt: 0
    #   stop execution and terminate the program
    if instr == 0:
        print("{}: halt".format(pc))
        pc += 1
    # set: 1 a b
    #   set register <a> to the value of <b>
    elif instr == 1:
        print("{}: set {} {}".format(pc, a, b))
        pc += 3
    # push: 2 a
    #   push <a> onto the stack
    elif instr == 2:
        print("{}: push {}".format(pc, a))
        pc += 2
    # pop: 3 a
    #   remove the top element from the stack and write it into <a>; empty stack = error
    elif instr == 3:
        print("{}: pop {}".format(pc, a))
        pc += 2
    # eq: 4 a b c
    #   set <a> to 1 if <b> is equal to <c>; set it to 0 otherwise
    elif instr == 4:
        print("{}: eq {} {} {}".format(pc, a, b, c))
        pc += 4
    # gt: 5 a b c
    #   set <a> to 1 if <b> is greater than <c>; set it to 0 otherwise
    elif instr == 5:
        print("{}: gt {} {} {}".format(pc, a, b, c))
        pc += 4
    # jmp: 6 a
    #   jump to <a>
    elif instr == 6:
        print("{}: jmp {}".format(pc, a))
        pc += 2
    # jt: 7 a b
    #   if <a> is nonzero, jump to <b>
    elif instr == 7:
        print("{}: jt {} {}".format(pc, a, b))
        pc += 3
    # jf: 8 a b
    #   if <a> is zero, jump to <b>
    elif instr == 8:
        print("{}: jf {} {}".format(pc, a, b))
        pc += 3
    # add: 9 a b c
    #   assign into <a> the sum of <b> and <c> (modulo 32768)
    elif instr == 9:
        print("{}: add {} {} {}".format(pc, a, b, c))
        pc += 4
    # mult: 10 a b c
    #   store into <a> the product of <b> and <c> (modulo 32768)
    elif instr == 10:
        print("{}: mult {} {} {}".format(pc, a, b, c))
        pc += 4
    # mod: 11 a b c
    #   store into <a> the remainder of <b> divided by <c>
    elif instr == 11:
        print("{}: mod {} {} {}".format(pc, a, b, c))
        pc += 4
    # and: 12 a b c
    #   stores into <a> the bitwise and of <b> and <c>
    elif instr == 12:
        print("{}: and {} {} {}".format(pc, a, b, c))
        pc += 4
    # or: 13 a b c
    #   stores into <a> the bitwise or of <b> and <c>
    elif instr == 13:
        print("{}: or {} {} {}".format(pc, a, b, c))
        pc += 4
    # not: 14 a b
    #   stores 15-bit bitwise inverse of <b> in <a>
    elif instr == 14:
        print("{}: not {} {}".format(pc, a, b))
        pc += 3
    # rmem: 15 a b
    #   read memory at address <b> and write it to <a>
    elif instr == 15:
        print("{}: rmem {} {}".format(pc, a, b))
        pc += 3
    # wmem: 16 a b
    #   write the value from <b> into memory at address <a>
    elif instr == 16:
        print("{}: wmem {} {}".format(pc, a, b))
        pc += 3
    # call: 17 a
    #   write the address of the next instruction to the stack and jump to <a>
    elif instr == 17:
        print("{}: call {}".format(pc, a))
        pc += 2
    # ret: 18
    #   remove the top element from the stack and jump to it; empty stack = halt
    elif instr == 18:
        print("{}: ret".format(pc))
        pc += 1
    # out: 19 a
    #   write the character represented by ascii code <a> to the terminal
    elif instr == 19:
        if isinstance(a, int):
            a = repr(chr(a))
        print("{}: out {}".format(pc, a))
        pc += 2
    # in: 20 a
    #   read a character from the terminal and write its ascii code to <a>; it can be assumed that once input starts, it will continue until a newline is encountered; this means that you can safely read whole lines from the keyboard and trust that they will be fully read
    elif instr == 20:
        print("{}: in {}".format(pc, a))
        pc += 2
    # noop: 21
    #  no operation
    elif instr == 21:
        print("{}: noop".format(pc))
        pc += 1
    else:
        print("{}: {} INVALID".format(pc, instr))
        pc += 1

    if pc > len(mem):
        exit
