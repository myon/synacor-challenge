#!/usr/bin/python3

import random

grid = [
        [ '*', 8, '-', 1 ],
        [ 4, '*', 11, '*' ],
        [ '+', 4, '-', 18 ],
        [ 22, '-', 9, '*'],
    ]

def run():
    x = 0
    y = 3
    track = []

    val = 22
    print(val, end='')
    op = None
    while True:
        (dx, dy, dir) = random.choice([(-1,0,'west'), (1,0,'east'), (0,-1,'north'), (0,1,'south')])
        if x+dx < 0 or x+dx > 3 or y+dy < 0 or y+dy > 3:
            continue

        x += dx
        y += dy
        if (x,y) == (0,3):
            print(" entry")
            return
        track.append(dir)

        item = grid[y][x]
        print(item, end='')
        if item in ('+', '-', '*'):
            op = item
        elif op == '+':
            val += item
        elif op == '-':
            val -= item
            if val < 0:
                print(" negative!")
                return
        elif op == '*':
            val *= item
        else:
            op = item

        if (x,y) == (3,0):
            print(" val is", val)
            if val == 30:
                [print(i) for i in track]
                exit(0)
            return

while True:
    run()
